﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QMP3RssImport_Application.RssImport;

namespace QMP3RssImport_Console
{
    class Program
    {

        static void Main(string[] args)
        {
            string paramList = string.Join(",", args);
            Console.WriteLine("PARA:" + paramList);
          //  paramList = "--s,as-pcua\\SQLEXPRESS,--d,upk31,--u,qmp,--p,qmp,--f,c:\\temp\\feedHAFL.xml,--z,15343,--t,title,--l,link,--n,description,--c,true,--y,0";
            string[] parameter = paramList.Split(',');

            string version = "1.0.0";

            Boolean log = true;
            // GET INIT PARAMETER
            Console.WriteLine("--------------------------------------------");
            Console.WriteLine("QM-Pilot 3.0 Import Links from RSS Feed " + version + "");
            Console.WriteLine("--------------------------------------------");
            string targetDBServer = null;
            string targetNameSql = null;
            string userName = null;
            string password = null;
            string rssfile = null;
            string title = null;
            string link = null;
            string nr = null;
            int debug = 0;
            bool subfolder = false;

            int parentId = 0;

            for (int i = 0; i < parameter.Length; i++)
            {
                if (!String.IsNullOrEmpty(parameter[i]) && parameter[i].Equals("--s") && parameter[i + 1] != null)
                {
                    targetDBServer = parameter[i + 1];
                    Console.WriteLine(targetDBServer);
                }
                else if (!String.IsNullOrEmpty(parameter[i]) && parameter[i].Equals("--d") && parameter[i + 1] != null)
                {
                    targetNameSql = parameter[i + 1];
                    Console.WriteLine(targetNameSql);
                }
                else if (!String.IsNullOrEmpty(parameter[i]) && parameter[i].Equals("--u") && parameter[i + 1] != null)
                {
                    userName = parameter[i + 1];
                }
                else if (!String.IsNullOrEmpty(parameter[i]) && parameter[i].Equals("--p") && parameter[i + 1] != null)
                {
                    password = parameter[i + 1];
                }
                else if (!String.IsNullOrEmpty(parameter[i]) && parameter[i].Equals("--f") && parameter[i + 1] != null)
                {
                    rssfile = parameter[i + 1];
                }
                else if (!String.IsNullOrEmpty(parameter[i]) && parameter[i].Equals("--z") && parameter[i + 1] != null)
                {
                    parentId = int.Parse(parameter[i + 1]);
                }
                else if (!String.IsNullOrEmpty(parameter[i]) && parameter[i].Equals("--t") && parameter[i + 1] != null)
                {
                    title = (parameter[i + 1]);
                }
                else if (!String.IsNullOrEmpty(parameter[i]) && parameter[i].Equals("--l") && parameter[i + 1] != null)
                {
                    link = (parameter[i + 1]);
                }
                else if (!String.IsNullOrEmpty(parameter[i]) && parameter[i].Equals("--n") && parameter[i + 1] != null)
                {
                    nr = (parameter[i + 1]);
                }
                else if (!String.IsNullOrEmpty(parameter[i]) && parameter[i].Equals("--y") && parameter[i + 1] != null)
                {
                    debug = int.Parse(parameter[i + 1]);
                }
                else if (!String.IsNullOrEmpty(parameter[i]) && parameter[i].Equals("--c") && parameter[i + 1] != null)
                {
                    subfolder = true;
                }
                
            }
            //path = "c:\\temp\\webapps\\a99\\a99a-www-qmpilot\\upload";
            //pathExcel = "c:\\temp\\webapps\\a99\\a99a-www-qmpilot\\excel\\FileList.xlsx";
            //targetDBServer = "as-pcua\\sql2008";
            //targetNameSql = "rehab3";
            //userName = "qmp";
            //password = "qmp";
            //deleteFromFileSystem = false;
            bool integratedSecurity = false;

            if (rssfile == null || targetDBServer == null || targetNameSql == null || userName == null || password == null || title == null || link == null || nr ==null || parentId==0)
            {
                Console.WriteLine("Please Enter all Parameters:");
                Console.WriteLine("--f c:\\temp\\feed.xml");
                Console.WriteLine("--s localhost\\MSSQLSERVER");
                Console.WriteLine("--d database");
                Console.WriteLine("--u username");
                Console.WriteLine("--p password");
                Console.WriteLine("--z ID from Parent Folder");
                Console.WriteLine("--t Label Propertie");
                Console.WriteLine("--l Link Propertie");
                Console.WriteLine("--n Nr Propertie");
                Console.WriteLine("--y Log Level 0-2");
                Console.WriteLine("--c Create Subfolders");
                Console.ReadLine();
                return;
            }

            RssImport thisFileImport = new RssImport();

            DateTime thisDateStart = DateTime.Now;
            Stopwatch thisWatch = new Stopwatch();
            thisWatch.Start();
            Console.WriteLine("-----------------------------------------------------");
            Console.WriteLine("QM-Pilot 3.0 RSS Link - Import started: " + thisDateStart);
            Console.WriteLine("QM-Pilot 3.0 Import RSS Link Toolkit Version " + version + "");
            Console.WriteLine("-----------------------------------------------------");
            Console.WriteLine("DB: " + targetDBServer + " / " + targetNameSql);
            Console.WriteLine("Username: " + userName + " Password: ***");
            Console.WriteLine("-----------------------------------------------------");

            thisFileImport.RunImport(targetDBServer, targetNameSql, userName, password, rssfile, integratedSecurity,  parentId,nr,title,link, subfolder,debug);

            DateTime thisDateEnd = DateTime.Now;
            thisWatch.Stop();
            Console.WriteLine("-----------------------------------------------------");
            Console.WriteLine("QM-Pilot 3.0  Import RSS Links finished: " + thisDateEnd + "  Duration: [" + (thisWatch.ElapsedMilliseconds) + "]");
            Console.WriteLine("-----------------------------------------------------");
            Console.Out.Flush();
            Console.WriteLine("Press Enter to exit");
            Console.ReadLine();
        }
    }
}
