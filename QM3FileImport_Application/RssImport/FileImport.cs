﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using QMP3RssImport_Application.Data;
using QMP3RssImport_Application.MigrationModel;
using System.Xml.Linq;


// Main Migration Function !
namespace QMP3RssImport_Application.RssImport
{
    public class RssImport
    {
        private int counterFile = 0;
        private int counterDir = 0;

        // Language Vars
        //private static List<language> OldLanguages = null;
        //private static List<GlobalLanguage> NewLanguages = null;
        //private static Dictionary<int,int> LanguageMap =  new Dictionary<int, int>();
        //private static List<int> AddLanguageMap = new List<int>(); 
        //private static language OldDefaultLang = null;
        //private static GlobalLanguage NewDefaultLang = null;

        private static List<Tuple<string, string>> Files = new List<Tuple<string, string>>();

        private static void GetFilesRecursive(string sDir)
        {
            try
            {
                //Console.WriteLine("Folder: " + sDir);
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    
                    GetFilesRecursive(d);
                    DoAction(sDir, null);
                }
                foreach (var file in Directory.GetFiles(sDir))
                {
                    //Console.WriteLine("File: " + file);
                    DoAction(sDir, file);
                }
            }
            catch (System.Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void DoAction(string folder, string filepath)
        {
            Files.Add(Tuple.Create(folder, filepath));
        }


        // Main Replication Function
        public void RunImport(string targetserver, string targetdb, string targetuser, string targetpwd, string path,  bool integratedSecurity, int parentId,string nr,string title,string link,bool subfolder,int debug)
        {
            int docCreateCounter = 0;
            int docUpdateCounter = 0;
            int ignoreCounter =0;
            int folderCreateCounter = 0;
            Console.Out.Flush();
            SqlConnectionStringBuilder  targetconnection = SetConnectionString(targetserver, targetdb, targetuser, targetpwd, integratedSecurity);
            if (testConnections(targetconnection)) {
                try {
                    XDocument doc = XDocument.Load(path);

                    if (doc != null)
                    {
                        var items = doc.Descendants("item");
                        foreach (var item in items)
                        {
                            var properties = item.Elements();
                            string docNr = null;
                            string docTitle = null;
                            string docLink = null;
                            foreach (var property in properties)
                            {
                                if (property.Name.ToString().ToLower().Equals(nr)) {
                                    docNr = property.Value.ToString();
                                    if (docNr != null)
                                    {
                                        if (docNr.IndexOf("QMPilot_DokNr") > -1)
                                        {
                                            docNr=docNr.Substring(docNr.IndexOf("QMPilot_DokNr"));
                                            docNr = docNr.Replace("QMPilot_DokNr:</b> ", "");
                                            docNr = docNr.Replace("</div>", "");
                                            docNr = docNr.Replace("\n", "");
                                        } else
                                        {
                                            docNr = null;
                                        }
                                        
                                    }
                                //    Console.WriteLine("NR:" + docNr);
                                } else if (property.Name.ToString().ToLower().Equals(title)) {
                                    docTitle = property.Value.ToString();
                                //    Console.WriteLine("TITLE:" + property.Value.ToString());
                                } else if (property.Name.ToString().ToLower().Equals(link)) {
                                    docLink = property.Value.ToString();
                                  //  Console.WriteLine("LINK:" + property.Value.ToString());
                                }
                            }
                            if (debug==2)
                            {
                                Console.WriteLine("Try to Create or Update:" + docNr + " | " + docTitle + " | " + docLink);
                            }
                            if (docNr!=null && docNr!=" " && docTitle != null && docLink != null)
                            {
                                int subFolderId = parentId;
                                if (subfolder)
                                {
                                    string folderName = docNr.Substring(0, docNr.Length-2);
                                    CoreDataVersions thisFolder = DataManagerCoreData.FolderExists(targetconnection, folderName);
                                    if (thisFolder != null)
                                    {
                                        subFolderId = thisFolder.CoreDataId;
                                    }
                                    else
                                    {
                                        subFolderId = createFolder(targetconnection, folderName, parentId, debug);
                                        folderCreateCounter++;
                                    }

                                }
                                
                                if (DataManagerCoreData.UpdateDocumentIfExists(targetconnection, docNr, docLink,docTitle, subFolderId))
                                {
                                    if (debug > 0)
                                    {
                                        Console.WriteLine("DOCUMENT UPDATED: " + docNr);
                                    }
                                    docUpdateCounter++;
                                } else
                                {
                                    docCreateCounter++;
                                    createDocument(targetconnection, docNr, docTitle, docLink, subFolderId, debug);
                                }
                            } else
                            {
                                ignoreCounter++;
                                if (debug > 0) { 
                                    Console.WriteLine("Element has not all Properties: Nr:" + docNr + " Title:" + docTitle + " Link:" + docLink);
                                }
                            }
                        }
                    }

                    //     BuildTree(targetconnection, path, pathExcel, deleteFromFileSystem, parentId);
                    //Console.WriteLine(counterDir + " Directory created");
                    //Console.WriteLine(counterFile + " Files created");
                    Console.WriteLine("UPDATED DOCUMENTS:" + docUpdateCounter);
                    Console.WriteLine("CREATED DOCUMENTS:" + docCreateCounter);
                    Console.WriteLine("IGNORED DOCUMENTS:" + ignoreCounter);
                    Console.WriteLine("CREATED FOLDERS:" + folderCreateCounter);
                    Console.Out.Flush();
                }
                catch (IOException ioe)
                {
                    Console.WriteLine(ioe.Message.ToString());
                }
            }
           
        }

        private int createDocument(SqlConnectionStringBuilder targetconnection,string docNr,string docTitle,string docLink,int parentId,int debug)
        {
            var newCoreData = new CoreDatas();
            newCoreData.Color = "#ffffff";
            newCoreData.CreationTime = DateTime.Now;
            newCoreData.MenuId = 1;
            newCoreData.DataType = 2;
            newCoreData.ParentId = parentId;
            newCoreData.Nr = docNr;
            newCoreData.Publish = true;
            int coreDataId = DataManagerCoreData.SaveCoreData(targetconnection, newCoreData);
            var newCoreDataVersion = new CoreDataVersions();
            newCoreDataVersion.CoreDataId = coreDataId;
            newCoreDataVersion.Label = docTitle;
            newCoreDataVersion.CreationTime = DateTime.Now;
            newCoreDataVersion.UpdateTime = DateTime.Now;
            newCoreDataVersion.CreateUserId = 1;
            newCoreDataVersion.UpdateUserId = 1;
            newCoreDataVersion.OwnUserId = 2;
            newCoreDataVersion.State = 4;
            newCoreDataVersion.CheckedOut = false;
            newCoreDataVersion.Version = 1.0f;
            newCoreDataVersion.Revision = 1;
            newCoreDataVersion.ValidFrom = DateTime.Now.AddDays(-1);
            newCoreDataVersion.OutputPdf = false;
            newCoreDataVersion.Link = docLink;
            newCoreDataVersion.GlobalLanguageId = 1;
            DataManagerCoreData.SaveCoreDataVersionForDocument(targetconnection, newCoreDataVersion);
            if (debug > 0)
            {
                Console.WriteLine("DOCUMENT CREATED: " + docNr);
            }
            return coreDataId;
        }


        private int createFolder(SqlConnectionStringBuilder targetconnection,  string docTitle, int parentId, int debug)
        {
            var newCoreData = new CoreDatas();
            newCoreData.Color = "#ffffff";
            newCoreData.CreationTime = DateTime.Now;
            newCoreData.MenuId = 1;
            newCoreData.DataType = 1;
            newCoreData.ParentId = parentId;
            newCoreData.Publish = true;
            int coreDataId = DataManagerCoreData.SaveCoreData(targetconnection, newCoreData);
            var newCoreDataVersion = new CoreDataVersions();
            newCoreDataVersion.CoreDataId = coreDataId;
            newCoreDataVersion.Label = docTitle;
            newCoreDataVersion.CreationTime = DateTime.Now;
            newCoreDataVersion.UpdateTime = DateTime.Now;
            newCoreDataVersion.CreateUserId = 1;
            newCoreDataVersion.UpdateUserId = 1;
            newCoreDataVersion.OwnUserId = 2;
            newCoreDataVersion.State = 4;
            newCoreDataVersion.CheckedOut = false;
            newCoreDataVersion.Version = 1.0f;
            newCoreDataVersion.Revision = 1;
            newCoreDataVersion.ValidFrom = DateTime.Now.AddDays(-1);
            newCoreDataVersion.OutputPdf = false;
            newCoreDataVersion.GlobalLanguageId = 1;
            DataManagerCoreData.SaveCoreDataVersionForFolder(targetconnection, newCoreDataVersion);
            if (debug > 0)
            {
                Console.WriteLine("FOLDER CREATED: " + docTitle);
            }
            return coreDataId;
        }


        private bool hasVersion(List<CoreDataObject> subContent)
        {
            foreach(CoreDataObject content in subContent)
            {
                if (content.thisVersion != null)
                {
                    return true;
                }
            }
            return false;
        }

        


        private Boolean testConnections(SqlConnectionStringBuilder targetconnection)
        {
            if (DataManagerCoreData.TestTargetConnection(targetconnection))
            {
                Console.WriteLine("Database connection ok");
                return true;
            }
            else
            {
                Console.WriteLine("Database connection error!");
            }
            return false;
        }

        // Build Connection String
        private SqlConnectionStringBuilder SetConnectionString(string datasource, string initialcatalog, string user, string pwd, bool integratedSecurity)
        {
            // create new connectionString 
            var connbuilder = new SqlConnectionStringBuilder
            {
                DataSource = datasource,
                InitialCatalog = initialcatalog,
                Password = pwd,
                UserID = user
            };

            // create new connectionString 
            if (integratedSecurity)
            {
                connbuilder = new SqlConnectionStringBuilder
                {
                    DataSource = datasource,
                    InitialCatalog = initialcatalog,
                    IntegratedSecurity = true
                };
            }

            return connbuilder;
        }
    }
}
