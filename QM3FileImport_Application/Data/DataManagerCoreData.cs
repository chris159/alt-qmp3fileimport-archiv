﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using Microsoft.Win32;
using QMP3RssImport_Application.MigrationModel;

namespace QMP3RssImport_Application.Data
{
    public static class DataManagerCoreData
    {

        /// <summary>
        /// //GET LIST OF GLOBALLANGUAGE IDS
        /// </summary>
        /// <param name="connString">CONNECTION</param>
        /// <returns>LIST OF GLOBALLANGUAGEIDS</returns>
        public static int GetDefaultLanguage(SqlConnectionStringBuilder connString)
        {
            using (var db = new DBQMP3DataContext(connString.ConnectionString))
            {
                return (from lang in db.GlobalLanguages where lang.DefaultLang==1  select lang.Id).FirstOrDefault();
            }
        }

        public static List<int> GetLanguageIds(SqlConnectionStringBuilder connString)
        {
            using (var db = new DBQMP3DataContext(connString.ConnectionString))
            {
                return (from lang in db.GlobalLanguages select lang.Id).ToList();
            }
        }

        public static Users FindUser(SqlConnectionStringBuilder connString, string usernameOrMail)
        {
            using (var db = new DBQMP3DataContext(connString.ConnectionString))
            {
                return (from us in db.Users where us.UserName.Equals(usernameOrMail) || us.EmailAddress.Equals(usernameOrMail) select us).FirstOrDefault();
            }
        }

        public static int FindDocumentType(SqlConnectionStringBuilder connString, string documentType)
        {
            using (var db = new DBQMP3DataContext(connString.ConnectionString))
            {
                return (from bdcontent in db.BdContentFields where bdcontent.FieldShort.Equals(documentType) && bdcontent.BdConfigTablesFieldId == 3 select bdcontent.BdContentId).FirstOrDefault();
            }
        }

        /// <summary>
        /// CHECK IF COREDATAVERSION WITH FILE EXISTS
        /// </summary>
        /// <param name="connString">CONNECTION</param>
        /// <param name="filename">FILENAME</param>
        /// <returns>COREDATAVERSION EXISTS</returns>
        public static bool FileExists(SqlConnectionStringBuilder connString, string filename)
        {
            using (var db = new DBQMP3DataContext(connString.ConnectionString))
            {
                var versions =
                    (from ver in db.CoreDataVersions where ver.Filename.Equals(filename)
                     select ver).ToList();
                return versions.Count > 0;
            }
        }

        /// <summary>
        /// CHECK IF FOLDER EXISTS
        /// </summary>
        /// <param name="connString">CONNECTION</param>
        /// <param name="foldername">FOLDERNAME</param>
        /// <returns>COREDATAVERSION</returns>
        public static CoreDataVersions FolderExists(SqlConnectionStringBuilder connString, string foldername)
        {
            using (var db = new DBQMP3DataContext(connString.ConnectionString))
            {
                var versions =
                    (from ver in db.CoreDataVersions
                     join cor in db.CoreDatas on ver.CoreDataId equals cor.Id
                     where cor.DataType == 1 && ver.Label.Equals(foldername)  
                     select ver).ToList();
                return versions.FirstOrDefault();
            }
        }


        public static bool UpdateDocumentIfExists(SqlConnectionStringBuilder connString, string docNr,string docLink,string docTitle,int oldParentId)
        {
            using (var db = new DBQMP3DataContext(connString.ConnectionString))
            {
                var versions =
                     (from ver in db.CoreDataVersions
                      join cor in db.CoreDatas on ver.CoreDataId equals cor.Id
                      where cor.DataType == 2 && cor.Nr.Equals(docNr) && cor.ParentId == oldParentId
                      select ver).ToList();
                CoreDataVersions thisDoc = versions.FirstOrDefault();
                if (thisDoc != null)
                {
                    thisDoc.Link = docLink;
                    thisDoc.Label = docTitle;
                    db.SubmitChanges();
                    return true;
                }
            }
            return false;
        }
       
        /// <summary>
        /// CHECK IF FOLDER EXISTS
        /// </summary>
        /// <param name="connString">CONNECTION</param>
        /// <param name="foldername">FOLDERNAME</param>
        /// <returns>COREDATAVERSION</returns>
        public static bool CheckIfChildren(SqlConnectionStringBuilder connString, CoreDataVersions version, CoreDataVersions parent)
        {
            using (var db = new DBQMP3DataContext(connString.ConnectionString))
            {
                var coredataId =
                    (from cor in db.CoreDatas
                        where cor.Id == version.CoreDataId
                        select cor.ParentId).FirstOrDefault();
                return coredataId == parent.CoreDataId;
            }
        }

        public static List<CoreDataObject> GetContent(SqlConnectionStringBuilder connString,int parentId, bool typeFilter, List<int> typeFilterList)
        {
            using (var db = new DBQMP3DataContext(connString.ConnectionString))
            {
                if (parentId > 0) {
                    List<CoreDataObject> thisObject = new List<CoreDataObject>();
                    if (!typeFilter)
                    {
                        thisObject = (from obj in db.CoreDatas
                                      select new MigrationModel.CoreDataObject
                                      {
                                          Id = obj.Id,
                                          ParentId = obj.ParentId,
                                          Nr = obj.Nr,
                                          DataType = obj.DataType,
                                          thisVersions = (from version in db.CoreDataVersions
                                                          where version.CoreDataId == obj.Id
                                                          select version).Where(u => u.State == 4 && u.GlobalLanguageId != 1).ToList(),
                                          thisVersion = (from version in db.CoreDataVersions
                                                         where version.CoreDataId == obj.Id
                                                         select version).Where(u => u.State == 4 && u.GlobalLanguageId == 1).FirstOrDefault()
                                      }).Where(u => u.ParentId == parentId && (u.DataType == 1 || u.DataType == 2)).ToList();
                    } else
                    {
                        thisObject = (from obj in db.CoreDatas
                                      select new MigrationModel.CoreDataObject
                                      {
                                          Id = obj.Id,
                                          ParentId = obj.ParentId,
                                          Nr = obj.Nr,
                                          DataType = obj.DataType,
                                          thisVersions = (from version in db.CoreDataVersions
                                                          where version.CoreDataId == obj.Id
                                                          select version).Where(u => u.State == 4 && u.GlobalLanguageId != 1 && (obj.DataType==1 || typeFilterList.Contains((int)u.BdContentId))).ToList(),
                                          thisVersion = (from version in db.CoreDataVersions
                                                         where version.CoreDataId == obj.Id
                                                         select version).Where(u => u.State == 4 && u.GlobalLanguageId == 1 && (obj.DataType == 1 || typeFilterList.Contains((int)u.BdContentId))).FirstOrDefault()
                                      }).Where(u => u.ParentId == parentId && (u.DataType == 1 || u.DataType == 2)).ToList();
                    }
                    return thisObject;
                } else
                {
                    List<CoreDataObject> thisObject =new List<CoreDataObject>();
                    if (!typeFilter)
                    {
                        thisObject = (from obj in db.CoreDatas
                                      select new MigrationModel.CoreDataObject
                                      {
                                          Id = obj.Id,
                                          ParentId = obj.ParentId,
                                          Nr = obj.Nr,
                                          DataType = obj.DataType,
                                          thisVersions = (from version in db.CoreDataVersions
                                                          where version.CoreDataId == obj.Id
                                                          select version).Where(u => u.State == 4 && u.Link == null && u.GlobalLanguageId != 1).ToList(),
                                          thisVersion = (from version in db.CoreDataVersions
                                                         where version.CoreDataId == obj.Id
                                                         select version).Where(u => u.State == 4 && u.Link == null && u.GlobalLanguageId == 1).FirstOrDefault()
                                      }).Where(u => u.ParentId == null && (u.DataType == 1 || u.DataType == 2)).ToList();
                    } else
                    {
                        thisObject = (from obj in db.CoreDatas
                                      select new MigrationModel.CoreDataObject
                                      {
                                          Id = obj.Id,
                                          ParentId = obj.ParentId,
                                          Nr = obj.Nr,
                                          DataType = obj.DataType,
                                          thisVersions = (from version in db.CoreDataVersions
                                                          where version.CoreDataId == obj.Id
                                                          select version).Where(u => u.State == 4 && u.Link == null && u.GlobalLanguageId != 1 && (obj.DataType == 1 || typeFilterList.Contains((int)u.BdContentId))).ToList(),
                                          thisVersion = (from version in db.CoreDataVersions
                                                         where version.CoreDataId == obj.Id
                                                         select version).Where(u => u.State == 4 && u.Link == null && u.GlobalLanguageId == 1 && (obj.DataType == 1 || typeFilterList.Contains((int)u.BdContentId))).FirstOrDefault()
                                      }).Where(u => u.ParentId == null && (u.DataType == 1 || u.DataType == 2)).ToList();
                    }
                    return thisObject;
                }
            }
            return null;
        }

        public static int SaveCoreData(SqlConnectionStringBuilder connString, CoreDatas coreDataToAdd)
        {
            using (var db = new DBQMP3DataContext(connString.ConnectionString))
            {
                db.CoreDatas.InsertOnSubmit(coreDataToAdd);
                db.SubmitChanges();
                var group = new CoreDataGroups();
                group.CoreDataId = coreDataToAdd.Id;
                group.EditCoreData = true;
                group.OpenCoreData = true;
                group.CreationTime = DateTime.Now;
                group.GroupId = 1;
                db.CoreDataGroups.InsertOnSubmit(group);
                db.SubmitChanges();
                return coreDataToAdd.Id;
            }
        }

        public static void SaveCoreDataVersionForDocument(SqlConnectionStringBuilder connString,
  CoreDataVersions coreDataVersionToAdd)
        {

            using (var db = new DBQMP3DataContext(connString.ConnectionString))
            {
                List<CoreDataVersions> versions = new List<CoreDataVersions>();
                var langIds = GetLanguageIds(connString);
                foreach (var id in langIds)
                {
                    var tempVersion = new CoreDataVersions();
                    tempVersion.CheckedOut = coreDataVersionToAdd.CheckedOut;
                    tempVersion.CoreDataId = coreDataVersionToAdd.CoreDataId;
                    tempVersion.Label = coreDataVersionToAdd.Label;
                    tempVersion.CreationTime = DateTime.Now;
                    tempVersion.UpdateTime = DateTime.Now;
                    tempVersion.CreateUserId = coreDataVersionToAdd.CreateUserId;
                    tempVersion.UpdateUserId = coreDataVersionToAdd.UpdateUserId;
                    tempVersion.OwnUserId = coreDataVersionToAdd.OwnUserId;
                    tempVersion.State = coreDataVersionToAdd.State;
                    tempVersion.Version = coreDataVersionToAdd.Version;
                    tempVersion.Revision = coreDataVersionToAdd.Revision;
                    tempVersion.ValidFrom = coreDataVersionToAdd.ValidFrom;
                    tempVersion.OutputPdf = coreDataVersionToAdd.OutputPdf;
                    tempVersion.GlobalLanguageId = id;
                    versions.Add(tempVersion);
                }

                db.CoreDataVersions.InsertOnSubmit(coreDataVersionToAdd);
                db.SubmitChanges();
            }
        }


        public static void SaveCoreDataVersionForFolder(SqlConnectionStringBuilder connString,
            CoreDataVersions coreDataVersionToAdd)
        {

            using (var db = new DBQMP3DataContext(connString.ConnectionString))
            {
                List<CoreDataVersions> versions = new List<CoreDataVersions>();
                var langIds = GetLanguageIds(connString);
                foreach (var id in langIds)
                {
                    var tempVersion = new CoreDataVersions();
                    tempVersion.CheckedOut = coreDataVersionToAdd.CheckedOut;
                    tempVersion.CoreDataId = coreDataVersionToAdd.CoreDataId;
                    tempVersion.Label = coreDataVersionToAdd.Label;
                    tempVersion.CreationTime = DateTime.Now;
                    tempVersion.UpdateTime = DateTime.Now;
                    tempVersion.CreateUserId = coreDataVersionToAdd.CreateUserId;
                    tempVersion.UpdateUserId = coreDataVersionToAdd.UpdateUserId;
                    tempVersion.OwnUserId = coreDataVersionToAdd.OwnUserId;
                    tempVersion.State = coreDataVersionToAdd.State;
                    tempVersion.Version = coreDataVersionToAdd.Version;
                    tempVersion.Revision = coreDataVersionToAdd.Revision;
                    tempVersion.ValidFrom = coreDataVersionToAdd.ValidFrom;
                    tempVersion.OutputPdf = coreDataVersionToAdd.OutputPdf;
                    tempVersion.GlobalLanguageId = id;
                    versions.Add(tempVersion);
                }

                db.CoreDataVersions.InsertAllOnSubmit(versions);
                db.SubmitChanges();
            }
        }

        public static byte[] ImageToByteArray(System.Drawing.Image image)
        {
            using (var ms = new MemoryStream())
            {
                image.Save(ms, image.RawFormat);
                return ms.ToArray();
            }
        }

        static string GetMimeType(FileInfo fileInfo)
        {
            const string unkownMimeType = "application/unknown";
            var regKey = Registry.ClassesRoot.OpenSubKey(fileInfo.Extension.ToLower());

            if (regKey == null)
                return unkownMimeType;

            var contentType = regKey.GetValue("Content Type");

            return (contentType == null) ? unkownMimeType : contentType.ToString();
        }


       
        public static Boolean TestTargetConnection(SqlConnectionStringBuilder connString)
        {
            try
            {

                using (var db = new DBQMP3DataContext(connString.ConnectionString))
                {
                    return db.DatabaseExists();
                }

            }
            catch (Exception e)
            {
                string message = e.Message;
                Console.WriteLine(message);
            }
            return false;
        }

    }
}
