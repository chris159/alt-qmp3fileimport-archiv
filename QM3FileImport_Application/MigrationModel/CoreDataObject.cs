﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QMP3RssImport_Application.Data;

namespace QMP3RssImport_Application.MigrationModel
{
    public partial class CoreDataObject : Data.CoreDatas
    {

        public List<CoreDataVersions> thisVersions { get; set; }
        public CoreDataVersions thisVersion { get; set; }
    }
}
